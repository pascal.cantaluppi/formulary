import sympy

n = sympy.Symbol('n')

expr = sympy.sqrt(4*n**2 - 1) / sympy.sqrt(2*n + 1)

limit_expr = sympy.limit(expr, n, sympy.oo)

print(limit_expr)

